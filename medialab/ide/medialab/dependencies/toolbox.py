#!/bin/python


def main():
    with open("toolbox.xml","r") as fp:
        file = open("toolbox.js","w")
        file.write('var toolbox = "";\n')
        for line in fp:
            line = line.replace('\'','\\\'')
            file.write('toolbox +='+'\''+line.replace('\n', "")+'\';\n')
        file.close()
main()
