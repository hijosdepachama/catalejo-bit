// Iniciar IOT y/o comandos
Blockly.Blocks['start_iot'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Iniciar");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("IOT/Tarea");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("./modulesMicropython/imag/iot.gif", 40, 40, "*"));
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(210);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Python['start_iot'] = function(block) {
  // TODO: Assemble Python into code variable.
  var code = 'from pyb import UART\n'+
	  'uart = UART(1,115200)\n'+
	  's2e = Pin(\'CONFIG\',Pin.OUT_PP)\n'+
	  'def command(dataToSend,sleepTime):\n'+
	  ' uart.writechar(ord(\'\\r\'))\n'+
	  ' pyb.delay(sleepTime)\n'+
	  ' s2e.high()\n'+
	  ' for x in dataToSend:\n'+
	  '  uart.writechar(ord(x))\n'+
	  '  pyb.delay(sleepTime)\n'+
	  ' s2e.low()\n'+
	  ' print(\'\')\n'+
	  '\n';
  return code;
};

//Tarea IOT
Blockly.Blocks['task_iot'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("IOT");
    this.appendValueInput("command")
        .appendField(new Blockly.FieldDropdown([["normal", "30"], ["rapido+", "10"], ["rapido", "20"], ["normal", "30"], ["lento", "40"], ["lento+", "50"], ["lento++", "60"]]), "wait");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(210);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Python['task_iot'] = function(block) {
  var dropdown_wait = block.getFieldValue('wait');
  var value_command = Blockly.Python.valueToCode(block, 'command', Blockly.Python.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  var code = 'command('+value_command+','+dropdown_wait+')\n';
  return code;
};

// MQTT iniciar
Blockly.Blocks['mqtt_init'] = {
  init: function() {
    this.appendValueInput("broker")
        .setCheck("String")
        .appendField("Iniciar MQTT")
        .appendField("Broker:");
    this.appendValueInput("port")
        .setCheck("String")
        .appendField("port:");
    this.appendValueInput("id")
        .setCheck("String")
        .appendField("id:");
    this.setInputsInline(true);
    this.setOutput(true, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Python['mqtt_init'] = function(block) {
  var value_broker = Blockly.Python.valueToCode(block, 'broker', Blockly.Python.ORDER_ATOMIC);
  var value_port = Blockly.Python.valueToCode(block, 'port', Blockly.Python.ORDER_ATOMIC);
  var value_id = Blockly.Python.valueToCode(block, 'id', Blockly.Python.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  var code = '\'MQTT_INIT\\r\'+str('+value_broker+')+\'\\r\'+str('+value_port+')+\'\\r\'+str('+value_id+')+\'\\r\'';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Python.ORDER_NONE];
};

// MQTT Subcripcion 
Blockly.Blocks['mqtt_sub1'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("MQTT");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Subcripción");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("mqtt_sub(a,m)");
    this.appendValueInput("topic1")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Asunto 1:");
    this.setOutput(true, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Python['mqtt_sub1'] = function(block) {
  var value_topic1 = Blockly.Python.valueToCode(block, 'topic1', Blockly.Python.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  var code = '...';
  var code = '\'MQTT_SUB\\r\'+str('+value_topic1+')+\'\\r\'';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Python.ORDER_NONE];
};

Blockly.Blocks['mqtt_sub2'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("MQTT");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Subcripción");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("mqtt_sub(a,m)");
    this.appendValueInput("topic1")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Asunto 1:");
    this.appendValueInput("topic2")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Asunto 2:");
    this.setOutput(true, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};
Blockly.Python['mqtt_sub2'] = function(block) {
  var value_topic1 = Blockly.Python.valueToCode(block, 'topic1', Blockly.Python.ORDER_ATOMIC);
  var value_topic2 = Blockly.Python.valueToCode(block, 'topic2', Blockly.Python.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable. 
  var code = '\'MQTT_SUB\\r\'+str('+value_topic1+')+\'\\r\'+str('+value_topic2+')+\'\\r\'';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Python.ORDER_NONE];
};

Blockly.Blocks['mqtt_sub3'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("MQTT");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Subcripción");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("mqtt_sub(a,m)");
    this.appendValueInput("topic1")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Asunto 1:");
    this.appendValueInput("topic2")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Asunto 2:");
    this.appendValueInput("topic3")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Asunto 3:");
    this.setOutput(true, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Python['mqtt_sub3'] = function(block) {
  var value_topic1 = Blockly.Python.valueToCode(block, 'topic1', Blockly.Python.ORDER_ATOMIC);
  var value_topic2 = Blockly.Python.valueToCode(block, 'topic2', Blockly.Python.ORDER_ATOMIC);
  var value_topic3 = Blockly.Python.valueToCode(block, 'topic3', Blockly.Python.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  var code = '\'MQTT_SUB\\r\'+str('+value_topic1+')+\'\\r\'+str('+value_topic2+')+\'\\r\'+str('+value_topic3+')+\'\\r\'';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Python.ORDER_NONE];
};

//Funcion de suscripcion
Blockly.Blocks['mqtt_sub_fun'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Función MQTT");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Suscripción");
    this.appendDummyInput()
        .appendField(new Blockly.FieldVariable("topic"), "topic")
        .appendField(new Blockly.FieldVariable("message"), "message");
    this.appendStatementInput("NAME")
        .setCheck(null);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Python['mqtt_sub_fun'] = function(block) {
  var variable_topic = Blockly.Python.variableDB_.getName(block.getFieldValue('topic'), Blockly.Variables.NAME_TYPE);
  var variable_message = Blockly.Python.variableDB_.getName(block.getFieldValue('message'), Blockly.Variables.NAME_TYPE);
  var statements_name = Blockly.Python.statementToCode(block, 'NAME');
  // TODO: Assemble Python into code variable.
  var code = '...';
  return code;
};

// Publicacion MQTT
Blockly.Blocks['mqtt_pub'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Publicar MQTT");
    this.appendValueInput("topic")
        .setCheck(null)
        .appendField("Asunto:");
    this.appendValueInput("message")
        .setCheck(null)
        .appendField("Mensaje:");
    this.setInputsInline(true);
    this.setOutput(true, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Python['mqtt_pub'] = function(block) {
  var value_topic = Blockly.Python.valueToCode(block, 'topic', Blockly.Python.ORDER_ATOMIC);
  var value_message = Blockly.Python.valueToCode(block, 'message', Blockly.Python.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  var code = '\'MQTT_PUB\\r\'+str('+value_topic+')+\'\\r\'+str('+value_message+')+\'\\r\'';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Python.ORDER_NONE];
};

// Escribir thingspeak 8 campos
Blockly.Blocks['write_thingspeak_fields8'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("ThingSpeak");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Escribir");
    this.appendValueInput("key")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Llave");
    this.appendValueInput("field1")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("field1:");
    this.appendValueInput("field2")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("field2:");
    this.appendValueInput("field3")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("field3:");
    this.appendValueInput("field4")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("field4:");
    this.appendValueInput("field5")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("field5:");
    this.appendValueInput("field6")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("field6:");
    this.appendValueInput("field7")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("field7:");
    this.appendValueInput("field8")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("field8:");
    this.setOutput(true, null);
    this.setColour(210);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};
Blockly.Python['write_thingspeak_fields8'] = function(block) {
  var value_key = Blockly.Python.valueToCode(block, 'key', Blockly.Python.ORDER_ATOMIC);
  var value_field1 = Blockly.Python.valueToCode(block, 'field1', Blockly.Python.ORDER_ATOMIC);
  var value_field2 = Blockly.Python.valueToCode(block, 'field2', Blockly.Python.ORDER_ATOMIC);
  var value_field3 = Blockly.Python.valueToCode(block, 'field3', Blockly.Python.ORDER_ATOMIC);
  var value_field4 = Blockly.Python.valueToCode(block, 'field4', Blockly.Python.ORDER_ATOMIC);
  var value_field5 = Blockly.Python.valueToCode(block, 'field5', Blockly.Python.ORDER_ATOMIC);
  var value_field6 = Blockly.Python.valueToCode(block, 'field6', Blockly.Python.ORDER_ATOMIC);
  var value_field7 = Blockly.Python.valueToCode(block, 'field7', Blockly.Python.ORDER_ATOMIC);
  var value_field8 = Blockly.Python.valueToCode(block, 'field8', Blockly.Python.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  var code = '\'TSW\\r\'+str('+value_field1+')+\'\\r\'+str('+value_field2+')+\'\\r\'+str('+value_field3+')+\'\\r\'+str('+value_field4+')+\'\\r\'+str('+value_field5+')+\'\\r\'+str('+value_field6+')+\'\\r\'+str('+value_field7+')+\'\\r\'+str('+value_field8+')+\'\\r\'';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Python.ORDER_NONE];
};

// Escribir thingspeak 6 campos

Blockly.Blocks['write_thingspeak_fields6'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("ThingSpeak");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Escribir");
    this.appendValueInput("key")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Llave");
    this.appendValueInput("field1")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("field1:");
    this.appendValueInput("field2")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("field2:");
    this.appendValueInput("field3")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("field3:");
    this.appendValueInput("field4")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("field4:");
    this.appendValueInput("field5")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("field5:");
    this.appendValueInput("field6")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("field6:");
    this.setOutput(true, null);
    this.setColour(210);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Python['write_thingspeak_fields6'] = function(block) {
  var value_key = Blockly.Python.valueToCode(block, 'key', Blockly.Python.ORDER_ATOMIC);
  var value_field1 = Blockly.Python.valueToCode(block, 'field1', Blockly.Python.ORDER_ATOMIC);
  var value_field2 = Blockly.Python.valueToCode(block, 'field2', Blockly.Python.ORDER_ATOMIC);
  var value_field3 = Blockly.Python.valueToCode(block, 'field3', Blockly.Python.ORDER_ATOMIC);
  var value_field4 = Blockly.Python.valueToCode(block, 'field4', Blockly.Python.ORDER_ATOMIC);
  var value_field5 = Blockly.Python.valueToCode(block, 'field5', Blockly.Python.ORDER_ATOMIC);
  var value_field6 = Blockly.Python.valueToCode(block, 'field6', Blockly.Python.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  var code = '\'TSW\\r\'+str('+value_field1+')+\'\\r\'+str('+value_field2+')+\'\\r\'+str('+value_field3+')+\'\\r\'+str('+value_field4+')+\'\\r\'+str('+value_field5+')+\'\\r\'+str('+value_field6+')+\'\\r\'';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Python.ORDER_NONE];
};

// Escribir thingspeak 4 campos
Blockly.Blocks['write_thingspeak_fields4'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("ThingSpeak");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Escribir");
    this.appendValueInput("key")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Llave");
    this.appendValueInput("field1")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("field1:");
    this.appendValueInput("field2")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("field2:");
    this.appendValueInput("field3")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("field3:");
    this.appendValueInput("field4")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("field4:");
    this.setOutput(true, null);
    this.setColour(210);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Python['write_thingspeak_fields4'] = function(block) {
  var value_key = Blockly.Python.valueToCode(block, 'key', Blockly.Python.ORDER_ATOMIC);
  var value_field1 = Blockly.Python.valueToCode(block, 'field1', Blockly.Python.ORDER_ATOMIC);
  var value_field2 = Blockly.Python.valueToCode(block, 'field2', Blockly.Python.ORDER_ATOMIC);
  var value_field3 = Blockly.Python.valueToCode(block, 'field3', Blockly.Python.ORDER_ATOMIC);
  var value_field4 = Blockly.Python.valueToCode(block, 'field4', Blockly.Python.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  var code = '\'TSW\\r\'+str('+value_field1+')+\'\\r\'+str('+value_field2+')+\'\\r\'+str('+value_field3+')+\'\\r\'+str('+value_field4+')+\'\\r\'';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Python.ORDER_NONE];
};

// Escribir thingspeak 2 campos

Blockly.Blocks['write_thingspeak_fields2'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("ThingSpeak");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Escribir");
    this.appendValueInput("key")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Llave");
    this.appendValueInput("field1")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("field1:");
    this.appendValueInput("field2")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("field2:");
    this.setOutput(true, null);
    this.setColour(210);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Python['write_thingspeak_fields2'] = function(block) {
  var value_key = Blockly.Python.valueToCode(block, 'key', Blockly.Python.ORDER_ATOMIC);
  var value_field1 = Blockly.Python.valueToCode(block, 'field1', Blockly.Python.ORDER_ATOMIC);
  var value_field2 = Blockly.Python.valueToCode(block, 'field2', Blockly.Python.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  var code = '\'TSW\\r\'+str('+value_field1+')+\'\\r\'+str('+value_field2+')+\'\\r\'';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Python.ORDER_NONE];
};
