# Uso de Branch en el repositorio #

**Ver si hay cambios en el remote branch**

```
git remote show origin
```

**Listar branch en el repositorio**

```
git branch
```

**Crear un branch**

```
git branch nuevo_branch
```

**Cambiar branch de trabajo**

```
git checkout nombre_branch
```

**Comentar los cambios en el branch**

```
git add .
git commit -m "mi comentario sobre cambios"
```

**Regresar al branch principal del proyecto.**

```
git checkout master
```

**Hacer las mezclas de las ramas al principal (debe hacerse el switch al principal**

```
git merge nombre_branch
```

**Subir los cambios al repositorio remoto**

```
git push origin nombre_branch
```

**Para crear un apuntador a un brach a mi repositorio**

```
git fetch origin branch:branch
```

**Hacer un pull de todos los branch**

```
git pull --all
```

**Ver branch que aparecen el el remoto**

```
git branch -a
```

**Traer un único branch**

```
git checkout -b <branch> --track <remote>/<branch>
```


